# mystery_number

User have 10 tries to find a random number between 0 and 1000.

This app is developped in the context of a Flutter course, drived by [Purple Giraffe](https://www.purplegiraffe.fr/)

## App configuration

* Languages supported : French / English
* Dark mode management
* Orientation fixed in portrait mode


