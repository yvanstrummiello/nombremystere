import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mystery_number/generated/l10n.dart';
import 'package:mystery_number/model/game_manager.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  var currentGame = GameManager();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    int _localNbChances = currentGame.nbChances;
    String _updatedMessage = currentGame.message;
    final ButtonStyle style = ElevatedButton.styleFrom(
        padding: const EdgeInsets.all(20),
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(20.00))),
        textStyle: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold));
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).title_displayed),
        centerTitle: true,
      ),
      body: SafeArea(
          child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            const SizedBox(height: 25),
            if (currentGame.isPlaying == false)
              Padding(
                padding: const EdgeInsets.only(right: 20.00, left: 20.00),
                child: Text(
                  S.of(context).instruction_text,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline6,
                ),
              ),
            if (currentGame.isPlaying == false) const SizedBox(height: 40),
            if (currentGame.isPlaying == false)
              Text(
                S.of(context).are_you_ready,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.headline3,
              ),
            if (currentGame.isPlaying == false) const SizedBox(height: 40),
            if (currentGame.isPlaying == false)
              ElevatedButton(
                  style: style,
                  onPressed: () {
                    setState(() {
                      currentGame.startGame();
                    });
                  },
                  child: Text(S.of(context).play_button)),
            if (currentGame.isPlaying == true)
              Text(S.of(context).tries_left(_localNbChances)),
            if (currentGame.isPlaying == true)
              Padding(
                padding: const EdgeInsets.only(
                    left: 100.00, right: 100.00, top: 50.00),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      TextFormField(
                        keyboardType: TextInputType.number,
                        inputFormatters: [
                          FilteringTextInputFormatter.digitsOnly
                        ],
                        autofocus: true,
                        maxLength: 4,
                        textAlign: TextAlign.center,
                        autocorrect: false,
                        validator: (String? value) {
                          if (value!.isEmpty) {
                            return S.of(context).error_message;
                          }
                          currentGame.pickUserValue(value);
                        },
                      ),
                      ElevatedButton(
                          style: style,
                          onPressed: () {
                            setState(() {
                              if (_formKey.currentState!.validate()) {
                                currentGame.controlUserValue();
                              }
                            });
                          },
                          child: Text(S.of(context).validation_button)),
                    ],
                  ),
                ),
              ),
            if (currentGame.isPlaying == true)
              Padding(
                padding: const EdgeInsets.only(top: 25.00, bottom: 25.00),
                child: Text(_updatedMessage),
              ),
            if (currentGame.isPlaying == true &&
                currentGame.gameIsFinished == true)
              ElevatedButton(
                  style: style,
                  onPressed: () {
                    setState(() {
                      currentGame.stopGame();
                    });
                  },
                  child: Text(S.of(context).replay_button))
          ],
        ),
      )),
    );
  }
}
