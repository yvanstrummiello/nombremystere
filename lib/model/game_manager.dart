import 'dart:math';

import 'package:mystery_number/generated/l10n.dart';

class GameManager {
  bool isPlaying = false;
  int _nbChances = 10;
  int? generatedNumber;
  int _userValue = 0;
  String _message = "";
  bool gameIsFinished = false;

  int get nbChances => _nbChances;
  String get message => _message;
  int get userValue => _userValue;

  set userValue(int value) {
    _userValue = userValue;
  }

  int _generateRandomNumber() {
    var random = Random();
    int randomNumber = random.nextInt(1000) + 1;
    return randomNumber;
  }

  startGame() {
    generatedNumber = _generateRandomNumber();
    isPlaying = true;
  }

  pickUserValue(value) {
    _userValue = int.parse(value);
  }

  controlUserValue() {
    if (_userValue > 1000) {
      _message = S.current.over_thousand_message;
    } else {
      if (_nbChances > 0) {
        if (_userValue > generatedNumber!) {
          _message = S.current.its_less;
          _decreaseNbChance();
        } else if (_userValue < generatedNumber!) {
          _message = S.current.its_more;
          _decreaseNbChance();
        } else {
          _message = S.current.winning_message;
          _isFinished();
        }
      } 
      if (_nbChances < 1) {
        _message = S.current.loosing_message(generatedNumber!);
        _isFinished();
      }
      
    }
  }

  _decreaseNbChance() {
    _nbChances--;
  }

  _isFinished(){
    gameIsFinished = true;
  }

  stopGame() {
    isPlaying = false;
    gameIsFinished = false;
    _nbChances = 10;
    _userValue = 0;
    _message = "";
  }
}
