// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class S {
  S();

  static S? _current;

  static S get current {
    assert(_current != null,
        'No instance of S was loaded. Try to initialize the S delegate before accessing S.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = S();
      S._current = instance;

      return instance;
    });
  }

  static S of(BuildContext context) {
    final instance = S.maybeOf(context);
    assert(instance != null,
        'No instance of S present in the widget tree. Did you add S.delegate in localizationsDelegates?');
    return instance!;
  }

  static S? maybeOf(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Mystery number`
  String get title_displayed {
    return Intl.message(
      'Mystery number',
      name: 'title_displayed',
      desc: '',
      args: [],
    );
  }

  /// `You have 10 tries to find a number between 1 and 1000`
  String get instruction_text {
    return Intl.message(
      'You have 10 tries to find a number between 1 and 1000',
      name: 'instruction_text',
      desc: '',
      args: [],
    );
  }

  /// `Play !`
  String get play_button {
    return Intl.message(
      'Play !',
      name: 'play_button',
      desc: '',
      args: [],
    );
  }

  /// `Play again`
  String get replay_button {
    return Intl.message(
      'Play again',
      name: 'replay_button',
      desc: '',
      args: [],
    );
  }

  /// `Validate`
  String get validation_button {
    return Intl.message(
      'Validate',
      name: 'validation_button',
      desc: '',
      args: [],
    );
  }

  /// `Please enter a valid number`
  String get error_message {
    return Intl.message(
      'Please enter a valid number',
      name: 'error_message',
      desc: '',
      args: [],
    );
  }

  /// `{localNbChances, plural, one{You have {localNbChances} more try} other{You have {localNbChances} more tries}}`
  String tries_left(num localNbChances) {
    return Intl.plural(
      localNbChances,
      one: 'You have $localNbChances more try',
      other: 'You have $localNbChances more tries',
      name: 'tries_left',
      desc: '',
      args: [localNbChances],
    );
  }

  /// `Ready ?`
  String get are_you_ready {
    return Intl.message(
      'Ready ?',
      name: 'are_you_ready',
      desc: '',
      args: [],
    );
  }

  /// `Please choose a number between 1 and 1000`
  String get over_thousand_message {
    return Intl.message(
      'Please choose a number between 1 and 1000',
      name: 'over_thousand_message',
      desc: '',
      args: [],
    );
  }

  /// `It's less....`
  String get its_less {
    return Intl.message(
      'It\'s less....',
      name: 'its_less',
      desc: '',
      args: [],
    );
  }

  /// `It's more....`
  String get its_more {
    return Intl.message(
      'It\'s more....',
      name: 'its_more',
      desc: '',
      args: [],
    );
  }

  /// `Congratulations ! You win !`
  String get winning_message {
    return Intl.message(
      'Congratulations ! You win !',
      name: 'winning_message',
      desc: '',
      args: [],
    );
  }

  /// `Sorry, you loose, the mystery number was {generatedNumber}`
  String loosing_message(Object generatedNumber) {
    return Intl.message(
      'Sorry, you loose, the mystery number was $generatedNumber',
      name: 'loosing_message',
      desc: '',
      args: [generatedNumber],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'fr'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
