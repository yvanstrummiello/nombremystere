// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a fr locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'fr';

  static String m0(generatedNumber) =>
      "Désolé, vous avez perdu, le nombre mystère était ${generatedNumber}";

  static String m1(localNbChances) =>
      "${Intl.plural(localNbChances, one: 'Vous avez encore ${localNbChances} essai', other: 'Vous avez encore ${localNbChances} essais')}";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "are_you_ready": MessageLookupByLibrary.simpleMessage("Prêt ?"),
        "error_message": MessageLookupByLibrary.simpleMessage(
            "Veuillez saisir un nombre valide"),
        "instruction_text": MessageLookupByLibrary.simpleMessage(
            "Vous avez 10 essais pour trouver un nombre entre 1 et 1000"),
        "its_less": MessageLookupByLibrary.simpleMessage("C\'est moins...."),
        "its_more": MessageLookupByLibrary.simpleMessage("C\'est plus...."),
        "loosing_message": m0,
        "over_thousand_message": MessageLookupByLibrary.simpleMessage(
            "Merci de choisir un chiffre entre 1 et 1000"),
        "play_button": MessageLookupByLibrary.simpleMessage("Jouer !"),
        "replay_button": MessageLookupByLibrary.simpleMessage("Rejouer"),
        "title_displayed":
            MessageLookupByLibrary.simpleMessage("Nombre Mystère"),
        "tries_left": m1,
        "validation_button": MessageLookupByLibrary.simpleMessage("Valider"),
        "winning_message":
            MessageLookupByLibrary.simpleMessage("Bravo ! C\'est gagné !")
      };
}
