// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  static String m0(generatedNumber) =>
      "Sorry, you loose, the mystery number was ${generatedNumber}";

  static String m1(localNbChances) =>
      "${Intl.plural(localNbChances, one: 'You have ${localNbChances} more try', other: 'You have ${localNbChances} more tries')}";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "are_you_ready": MessageLookupByLibrary.simpleMessage("Ready ?"),
        "error_message":
            MessageLookupByLibrary.simpleMessage("Please enter a valid number"),
        "instruction_text": MessageLookupByLibrary.simpleMessage(
            "You have 10 tries to find a number between 1 and 1000"),
        "its_less": MessageLookupByLibrary.simpleMessage("It\'s less...."),
        "its_more": MessageLookupByLibrary.simpleMessage("It\'s more...."),
        "loosing_message": m0,
        "over_thousand_message": MessageLookupByLibrary.simpleMessage(
            "Please choose a number between 1 and 1000"),
        "play_button": MessageLookupByLibrary.simpleMessage("Play !"),
        "replay_button": MessageLookupByLibrary.simpleMessage("Play again"),
        "title_displayed":
            MessageLookupByLibrary.simpleMessage("Mystery number"),
        "tries_left": m1,
        "validation_button": MessageLookupByLibrary.simpleMessage("Validate"),
        "winning_message":
            MessageLookupByLibrary.simpleMessage("Congratulations ! You win !")
      };
}
